<?php
/**
 * Defines the necessary constants and includes the necessary files for Thesis’ operation.
 *
 * Many WordPress customization tutorials suggest editing a theme’s functions.php file. With 
 * Thesis, you should edit the included custom/custom_functions.php file if you wish
 * to make modifications.
 *
 * @package Thesis
 * 1.8.5.2
 */
// Define directory constants

/* =REMOVE LINK FROM IMAGES IN POSTS
-------------------------------------------------------------- */
function remove_image_links( $content ) {

	$options = get_option( 'remove-image-links-options' );
	
	if( !$options ) {
		$options = array(
			'default_link' => 'none',
			'remove_jpg' => 1,
			'remove_jpeg' => 1,
			'remove_gif' => 1,
			'remove_png' => 1,
			'remove_bmp' => 1
		);
		

		add_option( 'remove-image-links-options', $options, '', 'no' );
	}
	

	$pattern = array();
	if( $options['remove_jpg'] ) array_push( $pattern, '/<a(.*?)href="(.*?).jpg"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
	if( $options['remove_jpeg'] ) array_push( $pattern, '/<a(.*?)href="(.*?).jpeg"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
	if( $options['remove_gif'] ) array_push( $pattern, 	'/<a(.*?)href="(.*?).gif"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
	if( $options['remove_png'] ) array_push( $pattern, 	'/<a(.*?)href="(.*?).png"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
	if( $options['remove_bmp'] ) array_push( $pattern, 	'/<a(.*?)href="(.*?).bmp"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
	

	$result = preg_replace( $pattern, '$4<img$5>$6', $content );
    return $result;
}

add_filter( 'the_content', 'remove_image_links' );

class RightSidebarAdsenseWidget extends WP_Widget {

	function RightSidebarAdsenseWidget() {
		// Instantiate the parent object
		parent::__construct( false, '336x280 Google Ad' );
	}

	function widget( $args, $instance ) {
		if ( !is_single() ):
			?>
		<script type="text/javascript"><!--
        google_ad_client = "ca-pub-6751808904033083";
        /* SS-Article R Sidebar */
        google_ad_slot = "1359930835";
        google_ad_width = 336;
        google_ad_height = 280;
        //-->
        </script>
        <script type="text/javascript"
        src="//pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
			<?php
	endif;
	}

	function update( $new_instance, $old_instance ) {
		print 'Updated.';
	}

	function form( $instance ) {
		print '';
	}
}

class RightSidebarAdsenseWidget2 extends WP_Widget {

	function RightSidebarAdsenseWidget2() {
		// Instantiate the parent object
		parent::__construct( false, '336x280 Google Ad 2' );
	}

	function widget( $args, $instance ) {
		if ( !is_single() ):
    ?>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- SS-Article Top R Sidebar -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:336px;height:280px"
             data-ad-client="ca-pub-6751808904033083"
             data-ad-slot="7291726430"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>

	<?php
	endif;
	}

	function update( $new_instance, $old_instance ) {
		print 'Updated.';
	}

	function form( $instance ) {
		print '';
	}
}

function myplugin_register_widgets() {
	register_widget( 'RightSidebarAdsenseWidget' );
    register_widget( 'RightSidebarAdsenseWidget2' );
}

add_action( 'widgets_init', 'myplugin_register_widgets' ); 

define('THESIS_LIB', TEMPLATEPATH . '/lib');
define('THESIS_ADMIN', THESIS_LIB . '/admin');
define('THESIS_CLASSES', THESIS_LIB . '/classes');
define('THESIS_FUNCTIONS', THESIS_LIB . '/functions');
define('THESIS_CSS', THESIS_LIB . '/css');
define('THESIS_HTML', THESIS_LIB . '/html');
define('THESIS_SCRIPTS', THESIS_LIB . '/scripts');
define('THESIS_IMAGES', THESIS_LIB . '/images');

if (isset($_REQUEST['template']) && isset($_REQUEST['stylesheet']) && (isset($_REQUEST['customize']) && $_REQUEST['customize'] === 'on') || (isset($_REQUEST['preview']) && ! empty($_REQUEST['preview'])))
	define('THESIS_NEW_WP_PREVIEW', true);

if (@file_exists(STYLESHEETPATH . "/custom"))
	define('THESIS_CUSTOM', STYLESHEETPATH . "/custom" . (is_multisite() && ! defined('THESIS_NEW_WP_PREVIEW') ? "-$blog_id" : ''));
else 
	define('THESIS_CUSTOM', TEMPLATEPATH . "/custom" . (is_multisite() && ! defined('THESIS_NEW_WP_PREVIEW') ? "-$blog_id" : ''));

// Define folder constants
define('THESIS_CSS_FOLDER', get_bloginfo('template_url') . '/lib/css'); #wp
define('THESIS_SCRIPTS_FOLDER', get_bloginfo('template_url') . '/lib/scripts'); #wp
define('THESIS_IMAGES_FOLDER', get_bloginfo('template_url') . '/lib/images'); #wp

if (@file_exists(THESIS_CUSTOM)) {
	if (@file_exists(STYLESHEETPATH . "/custom"))
		define('THESIS_CUSTOM_FOLDER', get_bloginfo('stylesheet_directory') . '/custom' . (is_multisite() && ! defined('THESIS_NEW_WP_PREVIEW') && get_option('thesis_design_options') ? "-$blog_id" : '')); #wp
	else
		define('THESIS_CUSTOM_FOLDER', get_bloginfo('template_url') . '/custom' . (is_multisite() && ! defined('THESIS_NEW_WP_PREVIEW') ? "-$blog_id" : '')); #wp
	define('THESIS_LAYOUT_CSS', THESIS_CUSTOM . '/layout.css');
	define('THESIS_ROTATOR', THESIS_CUSTOM . '/rotator');
	define('THESIS_ROTATOR_FOLDER', THESIS_CUSTOM_FOLDER . '/rotator');
}
elseif (file_exists(TEMPLATEPATH . '/custom-sample')) {
	define('THESIS_SAMPLE_FOLDER', get_bloginfo('stylesheet_directory') . '/custom-sample'); #wp
	define('THESIS_LAYOUT_CSS', TEMPLATEPATH . '/custom-sample/layout.css');
	define('THESIS_ROTATOR', TEMPLATEPATH . '/custom-sample/rotator');
	define('THESIS_ROTATOR_FOLDER', THESIS_SAMPLE_FOLDER . '/rotator');
}

// Load classes
require_once(THESIS_CLASSES . '/comments.php');
require_once(THESIS_CLASSES . '/css.php');
require_once(THESIS_CLASSES . '/data.php');
require_once(THESIS_CLASSES . '/favicon.php');
require_once(THESIS_CLASSES . '/head.php');
require_once(THESIS_CLASSES . '/javascript.php');
require_once(THESIS_CLASSES . '/loop.php');
require_once(THESIS_CLASSES . '/options_design.php');
require_once(THESIS_CLASSES . '/options_page.php');
require_once(THESIS_CLASSES . '/options_site.php');
require_once(THESIS_CLASSES . '/options_terms.php');
require_once(THESIS_CLASSES . '/widgets.php');

// Admin stuff
if (is_admin()) { #wp
	require_once(THESIS_ADMIN . '/file_editor.php');
	require_once(THESIS_ADMIN . '/fonts.php');
	require_once(THESIS_ADMIN . '/header_image.php');
	require_once(THESIS_ADMIN . '/options_manager.php');
	require_once(THESIS_ADMIN . '/options_post.php');
	require_once(THESIS_ADMIN . '/admin.php');
}

// Load template-based function files
require_once(THESIS_FUNCTIONS . '/comments.php');
require_once(THESIS_FUNCTIONS . '/compatibility.php');
require_once(THESIS_FUNCTIONS . '/content.php');
require_once(THESIS_FUNCTIONS . '/document.php');
require_once(THESIS_FUNCTIONS . '/feature_box.php');
require_once(THESIS_FUNCTIONS . '/helpers.php');
require_once(THESIS_FUNCTIONS . '/multimedia_box.php');
require_once(THESIS_FUNCTIONS . '/nav_menu.php');
require_once(THESIS_FUNCTIONS . '/post_images.php');
require_once(THESIS_FUNCTIONS . '/teasers.php');
require_once(THESIS_FUNCTIONS . '/version.php');
require_once(THESIS_FUNCTIONS . '/multisite.php');

// Load HTML frameworks
require_once(THESIS_HTML . '/content_box.php');
require_once(THESIS_HTML . '/footer.php');
require_once(THESIS_HTML . '/frameworks.php');
require_once(THESIS_HTML . '/header.php');
require_once(THESIS_HTML . '/hooks.php');
require_once(THESIS_HTML . '/sidebars.php');
require_once(THESIS_HTML . '/templates.php');

// Launch Thesis within WordPress
require_once(THESIS_FUNCTIONS . '/launch.php');

// include a sitewide custom_functions file in multisite
if (is_multisite() && @file_exists(TEMPLATEPATH . "/custom/custom_functions.php"))
	include_once(TEMPLATEPATH . "/custom/custom_functions.php");

// Include the user's custom_functions file, but only if it exists
if (@file_exists(THESIS_CUSTOM . '/custom_functions.php'))
	include_once(THESIS_CUSTOM . '/custom_functions.php');