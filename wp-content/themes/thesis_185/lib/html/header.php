<?php

function thesis_header_area() {
	thesis_hook_before_header();
	thesis_header();
	thesis_hook_after_header();
}

function thesis_header() {
	echo "\t<div id=\"header\">\n";
	thesis_hook_header();
    ?>
    <div class="social-media-box">
        <a href="https://www.facebook.com/pages/Surgery-Star/407029669419823" target="_blank">
            <img src="<?php print get_template_directory_uri(); ?>/img/facebook.png" width="30" class="social-media-button" />
        </a>
        <a href="https://plus.google.com/u/0/b/101001642843320582721/101001642843320582721/" target="_blank">
            <img src="<?php print get_template_directory_uri(); ?>/img/googleplus.png" width="30" class="social-media-button" />
        </a>
        <a href="http://www.pinterest.com/surgerystar/" target="_blank">
            <img src="<?php print get_template_directory_uri(); ?>/img/pinterest.png" width="30" class="social-media-button" />
        </a>
    </div>
    <?php
	echo "\t</div>\n";
}

function thesis_default_header() {
	thesis_hook_before_title();
	thesis_title_and_tagline();
	thesis_hook_after_title();
}