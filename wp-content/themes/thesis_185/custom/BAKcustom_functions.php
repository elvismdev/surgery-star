<?php
/* By taking advantage of hooks, filters, and the Custom Loop API, you can make Thesis
 * do ANYTHING you want. For more information, please see the following articles from
 * the Thesis User’s Guide or visit the members-only Thesis Support Forums:
 *
 * Hooks: http://diythemes.com/thesis/rtfm/customizing-with-hooks/
 * Filters: http://diythemes.com/thesis/rtfm/customizing-with-filters/
 * Custom Loop API: http://diythemes.com/thesis/rtfm/custom-loop-api/

---:[ place your custom code below this line ]:---*/
function next_random_article()
{
	global $post;

	session_start();

	$exclude_posts = isset( $_SESSION['seen_posts'] ) ? $_SESSION['seen_posts'] : array();

	if ( !in_array( $post->ID, $exclude_posts ) )
		array_push( $exclude_posts, $post->ID) ;

	$posts = get_posts( array('category__or' => array( 544,555 ), 'orderby' => 'RAND()', 'numberposts' => 1, 'post__not_in' => $exclude_posts ) );

	if ( !$posts )
	{
		$exclude_posts = array();
		$posts = get_posts( array('category__or' => array( 544,555 ), 'orderby' => 'RAND()', 'numberposts' => 1, 'post__not_in' => $exclude_posts ) );
	}

  $rand_post_id = $posts[0]->ID;

	$_SESSION['seen_posts'] = $exclude_posts;

  return $rand_post_id;
}

function str_insert($insertstring, $intostring, $offset)
{
    $part1 = substr($intostring, 0, $offset);
    $part2 = substr($intostring, $offset);
    $part1 = $part1 . $insertstring;
    $whole = $part1 . $part2;
    return $whole;

}

function next_prev_random_buttons( $next )
{
        $sHTML = '<div id="post_ad_middle">
						    		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
										<ins class="adsbygoogle" style="display:inline-block;width:468px;height:60px" data-ad-client="ca-pub-6751808904033083" data-ad-slot="9391764837"></ins>
										<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
									</div>';
		$sHTML .= '<div id="post-navigation">
								 <a href="' . $_SERVER['HTTP_REFERER'] . '"><img src="' . get_template_directory_uri() . '/img/previous.png" /></a>
                 <a href="' . get_permalink( $next ) . '"><img src="' . get_template_directory_uri() . '/img/next.png" /></a>
               </div>
               <div id="article-social">
								 <span class="st_facebook_hcount" displayText="Facebook"></span>
							 	 <span class="st_pinterest_hcount" displayText="Pinterest"></span>
							 	 <span class="st_stumbleupon_hcount" displayText="StumbleUpon"></span>
						   	 <span class="st_googleplus_hcount" displayText="Google +"></span>
						   	 <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
							 	 <script type="text/javascript">stLight.options({publisher: "072331cf-303c-4c3a-8758-1b48c7ffb4e0", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
							 </div>';
		$sHTML .= '<div style="clear:both"></div>';
	return $sHTML;
}

function first_post_image( $content )
{
    if( !is_single() || is_preview() )
    {
        return $content;
    }

    preg_match('#<img[^>]*>#i', $content, $match, PREG_OFFSET_CAPTURE);

    $length = strlen( $match[0][0] );

    $offset = $match[0][1] + $length;

    $insert = next_prev_random_buttons( next_random_article() );

		ob_start();
		thesis_get_sidebar();

		$sSidebar = ob_get_contents();
		ob_end_clean();

    $sHTML = str_insert( $insert . '<div id="sidebars">' . $sSidebar . '</div>', $content, $offset );

		$sHTML = '<div id="post_ad_left">
							   <!-- <a href="https://www.facebook.com/pages/Surgery-Star/407029669419823" target="_blank"><img src="' . get_template_directory_uri() . '/img/left-ad.jpg" alt="Like Us On facebook" /></a> -->
							 </div>' . $sHTML;

		/*$sHTML = '<div id="post_ad_left">
							   <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- SS-Article Top Left -->
                                <ins class="adsbygoogle"
                                     style="display:inline-block;width:336px;height:280px"
                                     data-ad-client="ca-pub-6751808904033083"
                                     data-ad-slot="8262795238"></ins>
                                <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
							 </div>' . $sHTML;*/


		$sHTML = '<div id="post_ad_right">
								 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
								 <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px" data-ad-client="ca-pub-6751808904033083" data-ad-slot="7915031636"></ins>
								 <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
							 </div>' . $sHTML;

		return $sHTML;
}

add_filter( 'the_content', 'first_post_image' );

// Remove original sidebars
function no_sidebars() {
	if (is_single())
		return false;
	else
		return true;
}
add_filter('thesis_show_sidebars', 'no_sidebars');
function ss_footer() {
        echo '<a href="/contact/">Contact</a> |';
        echo '<a href="/privacy-policy/">Privacy Policy</a> |';
        echo '<a href="/about-us/">About Us</a> |';
        echo '<a href="/dmca-disclaimer/">DMCA Disclaimer</a>';
}
add_filter( 'thesis_hook_footer', 'ss_footer' );
remove_action('thesis_hook_footer', 'thesis_attribution');

add_action('thesis_hook_after_html', 'ss_social_popup');
function ss_social_popup()
{
	echo '<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
				<script type="text/javascript" src="' . get_template_directory_uri() . '/custom/popup.js" ></script>';

	echo '<div id="overlay"></div>';

	echo '<div id="social_popup">
					<a href="#close-popup" id="close_social_popup">x</a>
					If you liked these before and after pictures of your favorite celebrity, like our facebook fan page for daily updates on plastic surgery and without makeup news!
					<br/>
					<br/>
					<span class="st_fblike_hcount" 		st_url="http://www.surgerystar.com/" displayText="Facebook"></span>
					<span class="st_twitter_hcount" 	st_url="http://www.surgerystar.com/" displayText="Tweet"></span>
					<span class="st_pinterest_hcount" st_url="http://www.surgerystar.com/" displayText="Pinterest"></span>
					<span class="st_plusone_hcount" 	st_url="http://www.surgerystar.com/" displayText="Google Plus"></span>

				</div>';

	echo '<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
				<script type="text/javascript">stLight.options({publisher: "ur-ebcc5ebb-9fec-e28a-ac90-11bd6bbfa32a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>';
}