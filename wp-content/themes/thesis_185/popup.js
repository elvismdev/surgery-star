function setCookie(cname,cvalue,exdays)
{
  var d = new Date();
  d.setTime(d.getTime()+(exdays*24*60*60*1000));
  var expires = "expires="+d.toGMTString();
  document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
}

function getCookie(cname)
{
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++)
  {
    var c = ca[i].trim();
    if (c.indexOf(name)==0) return c.substring(name.length,c.length);
  }
  return "";
}

function showPopup()
{
  jQuery(document).scrollTop(0,0);
  jQuery('#overlay').fadeIn();
  jQuery('#social_popup').fadeIn();
}

function closePopup()
{
  jQuery('#social_popup').fadeOut();
  jQuery('#overlay').fadeOut();
}

jQuery(document).ready( function() {

  jQuery('#overlay').click( function(){
    closePopup();
  });

  jQuery(document).on( 'click', '#close_social_popup', function(){
    closePopup();
  });

  pagesSeen = parseInt( getCookie('pagesSeen') );

  console.log( pagesSeen );

  if ( isNaN( pagesSeen )  )
    setCookie( 'pagesSeen', 1, 9999999999 );
  else
    setCookie( 'pagesSeen', pagesSeen + 1, 9999999999 );

  if ( pagesSeen == 1 ) 
    showPopup();
});