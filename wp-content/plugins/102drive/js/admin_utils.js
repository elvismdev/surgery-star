jQuery(document).ready( function(){

  // ADMIN
  jQuery('input, textarea').live( 'focus', function(){
    if ( jQuery(this).attr('data-default-value') )
    {
      defaultValue = jQuery(this).attr('data-default-value');

      if ( jQuery(this).val() == defaultValue )
      {
        jQuery(this).val('');
      }
    }
  });

  jQuery('input, textarea').live( 'blur', function(){
    if ( jQuery(this).attr('data-default-value') )
    {
      defaultValue = jQuery(this).attr('data-default-value');
      if ( jQuery(this).val() == '' )
      {
        jQuery(this).val( defaultValue );
      }
    }
  });

  // FRONTEND

});