DROP TABLE IF EXISTS `102drive_urls`;
CREATE TABLE `102drive_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `redirect_url` varchar(255) DEFAULT NULL,
  `do_410` tinyint(1) NOT NULL DEFAULT '0',
  `do_404` tinyint(1) NOT NULL DEFAULT '0',
  `do_302` tinyint(1) NOT NULL DEFAULT '0',
  `do_301` tinyint(1) NOT NULL DEFAULT '0',
  `noindex` tinyint(1) NOT NULL DEFAULT '0',
  `nofollow` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;