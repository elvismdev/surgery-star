<?php

	function url_tweaks_102drive()
	{
	  global $wpdb;

		$sAction = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : null;

		if ( $sAction )
		{
			switch ( $sAction )
			{
				case 'new':
					$aURLs = array();

					$sURLs = trim( $_POST['manual_urls'] );
					if ( $sURLs )
						$aURLs = explode( "\n", $sURLs );

					if ( $_FILES && isset( $_FILES['csv_urls'] ) && $_FILES['csv_urls']['tmp_name'] )
					{
						$fp = fopen( $_FILES['csv_urls']['tmp_name'], 'r' );
						$sCsvURLs = fread( $fp, filesize( $_FILES['csv_urls']['tmp_name'] ) );
						$aCsvRows = explode( "\n", $sCsvURLs );

						for( $i = 0; $i < count( $aCsvRows ); $i++ )
						{
							$aColumns = explode( ',', $aCsvRows[$i] );

							if ( trim( $aColumns[0] ) )
								$aURLs[] = trim( $aColumns[0] );
						}

						fclose( $fp );
					}

					$sUrlAction   = $_POST['url_action'];

					$iDo404       = $sUrlAction == '404'     ? 1 : 0;
					$iDo410       = $sUrlAction == '410'     ? 1 : 0;
					$iDo301       = $sUrlAction == '301'     ? 1 : 0;
					$iDo302       = $sUrlAction == '302'     ? 1 : 0;
					$iNoIndex     = $sUrlAction == 'noindex,nofollow' || $sUrlAction == 'noindex,follow' ? 1 : 0;
					$iNoFollow    = $sUrlAction == 'noindex,nofollow' || $sUrlAction == 'index,follow' ? 1 : 0;

					$sRedirectUrl = $_POST['redirect_url'];

					$sInsertQueryFormat = "INSERT INTO 102drive_urls (url, redirect_url, do_404, do_410, do_302, do_301, noindex, nofollow) VALUES ( '%s', '$sRedirectUrl', $iDo404, $iDo410, $iDo302, $iDo301, $iNoIndex, $iNoFollow )";
					$sUpdateQueryFormat = "UPDATE 102drive_urls SET redirect_url = '$sRedirectUrl', do_404 = $iDo404, do_410 = $iDo410, do_302 = $iDo302, do_301 = $iDo301, noindex = $iNoIndex WHERE url = '%s'";
					foreach ( $aURLs as &$sURL )
					{
						$aUrlParts = parse_url( trim( $sURL ) );
						$sURL = strtolower( $aUrlParts['path'] . ( $aUrlParts['query'] ? '?' . $aUrlParts['query'] : '' ) );

						if ( count( $wpdb->get_results( $wpdb->prepare( "SELECT id FROM 102drive_urls WHERE url = '%s'", $sURL ) ) ) )
							$wpdb->query( $wpdb->prepare( $sUpdateQueryFormat, $sURL ) );
						else
							$wpdb->query( $wpdb->prepare( $sInsertQueryFormat, $sURL ) );
					}
				break;

				case 'edit':
					$iURL = $_REQUEST['id'];

				break;

				case 'remove':
					$iURL = $_REQUEST['id'];

					$wpdb->query( "DELETE FROM 102drive_urls WHERE id = $iURL" );
				break;
			}
		}

		$sQuery = isset( $_REQUEST['keywords'] ) ? $_REQUEST['keywords'] : '';

		$iPage = isset( $_REQUEST['paged'] ) ? $_REQUEST['paged'] : 1;
		$iOffset = ( $iPage - 1 ) * 500;

		if ( $sQuery )
			$aTotalPages = $wpdb->get_results( "SELECT count(*) as count FROM 102drive_urls WHERE url LIKE '%$sQuery%'" );
		else
			$aTotalPages = $wpdb->get_results( "SELECT count(*) as count FROM 102drive_urls" );

		$iTotalPages = ceil( $aTotalPages[0]->count / 500 );

		if ( $sQuery )
			$aURLs = $wpdb->get_results( "SELECT * FROM 102drive_urls WHERE url LIKE '%$sQuery%' LIMIT $iOffset, 500" );
		else
			$aURLs = $wpdb->get_results( "SELECT * FROM 102drive_urls LIMIT $iOffset, 500" );

		require_once( WP_PLUGIN_DIR . '/102drive/templates/urls.tpl' );
	}

	function url_tweaks_add_102drive()
	{
		require_once( WP_PLUGIN_DIR . '/102drive/templates/add_urls.tpl' );
	}


?>