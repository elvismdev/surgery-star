<?php
function general_settings_102drive()
{
  echo '<div class="wrap">
          <h1>102Drive General Settings</h1>';

  if ( $_REQUEST['submit'] )
      update_102drive_general_settings();

  print_102drive_general_settings_form();

  echo '</div>';
}

function update_102drive_general_settings()
{
  $bUpdated = false;
  if ( isset( $_REQUEST['102drive_nofollow_external_links'] ) )
  {
     update_option( '102drive_nofollow_external_links',  '1' );
     $bUpdated = true;
  }
	else
	{
		update_option( '102drive_nofollow_external_links',  '0' );
		$bUpdated = true;
	}

  
  if ( $bUpdated ) 
  {
      echo '<div id="message" class="updated fade">';
      echo '  <p>Settings successfully updated!</p>';
      echo '</div>';
   } 
   else 
   {
      echo '<div id="message" class="error fade">';
      echo '  <p>Sorry, we were unable to update your settings!</p>';
      echo '</div>'; 
   }
}

function print_102drive_general_settings_form()
{
   $bNoFollowExternalLinks = get_option( '102drive_nofollow_external_links' ) == '1' ? true : false;

   echo '<br/>
         <form method="post">
            <input type="checkbox" name="102drive_nofollow_external_links" value="1" '. ( $bNoFollowExternalLinks ? 'checked="checked"' : '' ) .' /> NOFOLLOW all external links on posts
            <br/>
            <br/>
            <input type="submit"  name="submit" class="button button-primary button-large"  value="Apply!" />
         </form>';
}

?>