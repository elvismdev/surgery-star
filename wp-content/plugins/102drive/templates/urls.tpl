<div class="wrap">
	<form id="102drive_urls_search_form" action="" method="post" style="float: right">
		<input type="text" name="keywords" data-default-value="Search URLs..." value="<?= $sQuery ? $sQuery : 'Search URLs...' ?>" />
		<input type="submit" value="Go" />
	</form>
	<h2>102Drive URL SEO Tweaks <a href="?page=102drive-url-tweaks-add" class="add-new-h2">Add URLs</a></h2>
	<br/>
	<table class="wp-list-table widefat fixed posts">
		<tr>
			<th>URL</th>
			<th>No Index</th>
			<th>No Follow</th>
			<th>410 Gone</th>
			<th>404 Not Found</th>
			<th>302 Moved Temporarily to</th>
			<th>301 Moved Permanently to</th>
			<th>Actions</th>
		</tr>
		<?php foreach( $aURLs as $iIdx => $oURL ): ?>
		  <tr class="<?php if ( $iIdx % 2 == 0 ): ?> alternate <?php endif; ?>">
			  <td><a href="<?= $oURL->url ?>" target="_blank"><?= $oURL->url ?></a></td>
			  <td><?= ( $oURL->noindex ? '<b>Y</b>' : '-' ) ?></td>
			  <td><?= ( $oURL->nofollow ? '<b>Y</b>' : '-' ) ?></td>
			  <td><?= ( $oURL->do_410 ? '<b>Y</b>' : '-' ) ?></td>
			  <td><?= ( $oURL->do_404 ? '<b>Y</b>' : '-' ) ?></td>
			  <td><?= ( $oURL->do_302 ? '<a href="' . $oURL->redirect_url . '" target="_blank">' . $oURL->redirect_url  . '</a>' : '-' ) ?></td>
			  <td><?= ( $oURL->do_301 ? '<a href="' . $oURL->redirect_url . '" target="_blank">' . $oURL->redirect_url  . '</a>' : '-' ) ?></td>
			  <td><a href="?page=102drive-url-tweaks&action=remove&id=<?= $oURL->id ?>&keywords=<?= $sQuery ?>">Remove</a></td>
		  </tr>
		<?php endforeach; ?>
	</table>
	<br/>
	<div class="tablenav">
		<div class="tablenav-pages">
			<span class="pagination-links">
				<?php if ( $iPage > 1 ): ?>
					<a class="first-page" title="Go to the first page" href="?page=102drive-url-tweaks">&laquo;</a>
					<a class="prev-page" title="Go to the previous page" href="?page=102drive-url-tweaks&paged=<?= $iPage - 1 ?>&keywords=<?= $sQuery ?>">‹ Previous</a>
				<?php endif; ?>

				<?php if ( $iPage < $iTotalPages ): ?>
					<a class="next-page" title="Go to the next page" href="?page=102drive-url-tweaks&paged=<?= $iPage + 1 ?>&keywords=<?= $sQuery ?>">Next ›</a>
					<a class="last-page" title="Go to the last page" href="?page=102drive-url-tweaks&paged=<?= $iTotalPages ?>&keywords=<?= $sQuery ?>">&raquo;</a>
				<?php endif; ?>

			</span>
		</div>
	</div>
</div>