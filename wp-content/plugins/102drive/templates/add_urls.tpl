<div class="wrap">
	<h2>Add URLs</h2>
	<form action="?page=102drive-url-tweaks&action=new" method="post" enctype="multipart/form-data">
		<label for="102drive_import_urls">Import as CSV: </label>
		<input type="file" name="csv_urls" id="102drive_import_urls" />
		<span class="description">Any CSV format is permitted as long as the URL is in the first column.</span>
		<h3>- AND / OR -</h3>
		<label for="102drive_add_urls">Manually add URLs:</label>
		<textarea id="102drive_add_urls" name="manual_urls" tabindex="1" name="text" rows="10" style="width: 100%"></textarea>
		<span class="description">Enter one URL per line, can use absolute or relative URLs starting with "/".</span>
		<br/>
		<br/>
		<b>With the above URLs do:</b>
		<br/>
		<br/>
		<input type="radio" name="url_action" value="noindex,nofollow" checked id="url_action_noindex_nofollow" /> <label for="url_action_noindex_nofollow">NOINDEX,NOFOLLOW</label>
		<br/>
		<input type="radio" name="url_action" value="noindex,follow"   id="url_action_noindex_follow" />   <label for="url_action_noindex_follow">NOINDEX,FOLLOW</label>
		<br/>
		<input type="radio" name="url_action" value="index,nofollow"   id="url_action_index_nofollow" />   <label for="url_action_index_nofollow">INDEX,NOFOLLOW</label>
		<br/>
		<br/>
		<input type="radio" name="url_action" value="410" id="url_action_410"/> <label for="url_action_410">410 Gone</label>
		<br/>
		<input type="radio" name="url_action" value="404" id="url_action_404" /> <label for="url_action_404">404 Not Found</label>
		<br/>
		<input type="radio" name="url_action" value="301" id="url_action_301" /> <label for="url_action_301">301 Moved Permanently</label>
		<br/>
		<input type="radio" name="url_action" value="302" id="url_action_302" /> <label for="url_action_302">302 Moved Temporarily</label>
		<br/>
		<br/>
		<label for="redirect_url">Redirect URL:</label> <input type="text" name="redirect_url" id="redirect_url" size="100" />
		<br/>
		<span class="description">Optional: Only required for 301 and 302 actions. Can be an absolute URL or relative URL starting with "/".</span>
		<br/>
		<br/>
		<input type="hidden" name="action" value="new" />
		<input type="submit" value="Apply" class="button button-primary button-large" />
	</form>
</div>