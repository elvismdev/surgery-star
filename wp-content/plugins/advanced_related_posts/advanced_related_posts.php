<?php
/**
 * Plugin Name: Advanced Related Posts
 * Plugin URI: http://thesistut.com/store/advanced-related-posts/
 * Description: Easy way to display related posts with thumbnails on your site. Supports custom post types and custom taxonomies, extremely flexible and has a really handy user interface.
 * Version: 1.0.1
 * Author: Serge Liatko
 * Author URI: http://thesistut.com/
 * Text Domain: advanced_related_posts
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

class advanced_related_posts {

	function __construct() {
		$this->_define_uploads_url_base();
		add_action('init',array($this,'init'),50,0);
		add_action('admin_init',array($this,'admin_init'),50,0);
		add_action('admin_menu',array($this,'admin_menu'),10,0);
		add_action('add_meta_boxes',array($this,'add_meta_boxes'));
		add_action('save_post',array($this,'save_post'));
		add_filter('plugin_action_links', array($this,'plugin_action_links'), 10, 2 );
		add_action('plugins_loaded',array($this,'plugins_loaded'),10,0);
	}
	
	/* load text domain */
	public function plugins_loaded() {
		load_plugin_textdomain( 'advanced_related_posts', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
	
	/* START INIT */
	public function init() {
		add_action('wp_head',array($this,'wp_head'),50,0);
		add_action('wp_enqueue_scripts',array($this,'front_style'),10,0);
	}
	public function front_style() {
		if(is_singular() && !is_singular('page')) {
			global $post;
			$options = get_option('advanced_related_posts_options');
			$hide = get_post_meta($post->ID, '_avanced_related_posts_hide', true);
			if(empty($hide) && !empty($options["{$post->post_type}"]["display"]) && empty($options["css_hide"])) {
				wp_register_style( 'avanced_related_posts_front_css', plugins_url('/style.css', __FILE__), array(), false, 'all' );
				wp_enqueue_style('avanced_related_posts_front_css');
			}
		}
	}
	/* END INIT */
	
	/* START FRONT */
	public function wp_head() {
		if(is_singular() && !is_singular('page')) {
			global $post;
			$options = get_option('advanced_related_posts_options');
			$hide = get_post_meta($post->ID, '_avanced_related_posts_hide', true);
			if(empty($hide) && !empty($options["{$post->post_type}"]["display"]) && (!empty($options["{$post->post_type}"]["thumbnail_display"]) || !empty($options["{$post->post_type}"]["title_display"]))) {
				$filter_order = (!empty($options["{$post->post_type}"]["filter_order"])) ? intval($options["{$post->post_type}"]["filter_order"]) : 11 ;
				add_filter('the_content', array($this, 'the_content'), $filter_order, 1);
			}
		}
	}
	public function the_content($content = null) {
		global $post;
		$options = get_option('advanced_related_posts_options');
		$option = (is_array($options["{$post->post_type}"]) && !empty($options["{$post->post_type}"])) ? $options["{$post->post_type}"] : array();
		/* initial check */
		if(is_array($option) && (empty($option['post_type']) || empty($option['taxonomy']))) {
			return $content;
		}
		$data = wp_parse_args($option, array(
			'box_html_id' => '',
			'box_css_class' => '',
			'posts_per_page' => '1',
			'post_type' => 'post',
			'taxonomy' => 'category',
			'introduction_text' => '',
			'introduction_html_tag' => 'p',
			'introduction_css_class' => '',
			'thumbnail_display' => '',
			'thumbnail_size' => 'thumbnail',
			'thumbnail_custom_size' => array("75","75"),
			'thumbnail_css_class' => '',
			'thumbnail_search' => '',
			'thumbnail_search_meta_fields' => '',
			'thumbnail_default_url' => '',
			'title_display' => '',
			'title_max_length' => '',
			'link_title_prefix' => '',
			'link_css_class' => '',
			'link_relation' => '',
			'link_inline' => ''
		));
		extract($data, EXTR_SKIP);
		$terms = wp_get_object_terms($post->ID, $taxonomy, array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'ids'));
		if(empty($terms) || !is_array($terms) || is_wp_error($terms)) {
			return $content;
		}
		$items = get_posts(array(
			'post_type' => $post_type,
			'posts_per_page' => $posts_per_page,
			'post_status' => 'publish',
			'orderby' => 'rand',
			'post__not_in' => array($post->ID),
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field' => 'id',
					'terms' => $terms,
					'include_children' => true
				)
			)
		));
		if(empty($items) || !is_array($items) || is_wp_error($items)) {
			return $content;
		}
		/* start construction */
		$out = $introduction = "";
		$count = 0 ;
		foreach($items as $item) {
			$count ++;
			$item_url = $item_title = $item_name = $item_img = $img_id = '';
			$item_url = get_permalink($item->ID);
			$item_name = apply_filters('the_title',$item->post_title, $item->ID);
			$item_title = esc_attr($item_name);
			$link_title = (!empty($link_title_prefix)) ? esc_attr($link_title_prefix) . $item_title : $item_title ;
			$link_relation_attribute = (!empty($link_relation)) ? " rel=\"". esc_attr($link_relation) . "\"" : "";
			$link_css_class_attribute = (!empty($link_css_class)) ? " class=\"". esc_attr($link_css_class) . "\"" : "";
			$link_inline_attribute = (!empty($link_inline)) ? " ". strip_tags($link_inline) : "";
			$link_before = "<a href=\"{$item_url}\" title=\"{$link_title}\"{$link_relation_attribute}{$link_css_class_attribute}{$link_inline_attribute} >";
			$link_text = (!empty($title_display)) ? $item_name : "";
			$link_text = (!empty($title_max_length)) ? advanced_related_posts_shorten_title($link_text,intval($title_max_length)) : $link_text;
			$link_after = "{$link_text}</a>";
			/* get image if needed */
			if(!empty($thumbnail_display)) {
				if(has_post_thumbnail($item->ID)) {
					$img_id = get_post_thumbnail_id($item->ID);
				}
				else {
					if(!empty($thumbnail_search)) {
						$img_id = $this->get_attachment_id_from_url($this->catch_img_src($item, $thumbnail_default_url, $thumbnail_search_meta_fields)) ;
					}
				}
				if(!empty($img_id)) {
					$attr = array(
						'class' => ((!empty($thumbnail_css_class)) ? esc_attr($thumbnail_css_class) : "" ),
						'alt' => trim(strip_tags($item_title)),
						'title' => ""
					);
					$img_size = (!empty($thumbnail_size)) ? $thumbnail_size : "thumbnail";
					if('custom' == $img_size) {
						$img_size = (!empty($thumbnail_custom_size) && is_array($thumbnail_custom_size)) ? $thumbnail_custom_size : array(75,75);
					}
					$item_img = wp_get_attachment_image( $img_id, $img_size, false, $attr );
				}
			}
			$link = $link_before . $item_img . $link_after ;
			$out .= "<div class=\"related_post related_post_{$count}\">\n{$link}\n<div class=\"clearfix\"></div>\n</div>\n";
		}
		if(!empty($introduction_text)) {
			$introduction_class = (!empty($introduction_css_class)) ? " class=\"". trim(strip_tags(esc_attr($introduction_css_class))) ."\"" : "";
			$introduction = "<{$introduction_html_tag}{$introduction_class}>{$introduction_text}</{$introduction_html_tag}>\n";
		}
		$wrapper_id = (!empty($box_html_id)) ? " id=\"". trim(strip_tags(esc_attr($box_html_id))) ."\"" : "";
		$wrapper_class = (!empty($box_css_class)) ? " class=\"". trim(strip_tags(esc_attr($box_css_class))) ."\"" : "";
		$out = "<div class=\"related_posts related_posts_by_{$count}_columns\">\n{$out}\n<div class=\"clearfix\"></div>\n</div>\n";
		$out = "<div{$wrapper_id}{$wrapper_class}>\n{$introduction}\n{$out}\n<div class=\"clearfix\"></div>\n</div>\n";
		/* return the result */
		return $content . $out;
	}
	/* front helpers */
	public function _define_uploads_url_base() {
		/* get the media library dir */
		if(!defined('UPLOADS_BASE_URL')) {
			$upload_dir = wp_upload_dir();
			define('UPLOADS_BASE_URL', $upload_dir['baseurl']);
		}
	}
	public function catch_img_src($post = null, $default_url = '', $thumbnail_search_meta_fields = null) {
		if(is_object($post)) {
			/* start by looking in custom fields */
			if(!empty($thumbnail_search_meta_fields) && is_string($thumbnail_search_meta_fields)) {
				$fields = explode(',',$thumbnail_search_meta_fields);
				foreach($fields as $field) {
					$data = get_post_meta($post->ID, $field, true);
					if(!empty($data) && is_string($data) && strstr($thumb_two, UPLOADS_BASE_URL)) {
						return $data ;
					}
				}
			}
			/* continue looking in the post content */
			preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', do_shortcode($post->post_content), $matches); 
			if(is_array($matches[1])) {
				foreach($matches[1] as $url) {
					if(!empty($url) && strstr($url, UPLOADS_BASE_URL)) {
						return $url; 
					}
				}
			}
		}
		return $default_url;
	}
	public function get_attachment_id_from_url($attachment_url = '') {
		global $wpdb;
		$attachment_id = false;
		if ( '' == $attachment_url ) return;
		$upload_dir_paths = wp_upload_dir();
		if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {
			$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );
			$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
			$attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts, $wpdb->postmeta WHERE ID = post_id AND meta_key = '_wp_attached_file' AND meta_value = '%s' AND post_type = 'attachment'", $attachment_url ) );
		}
		return $attachment_id;
	}
	/* END FRONT */	
	
	/* START ADMIN */
	function plugin_action_links($links, $file) {
		if ($file == plugin_basename(__FILE__)){
			$url = admin_url() ;
			$settings_link = '<a href="'. $url .'themes.php?page=avanced_related_posts_options_page">' . __('Settings') . '</a>';
			array_unshift($links, $settings_link);
		}
		return $links;
	}
	public function admin_init() {
		register_setting('advanced_related_posts_group','advanced_related_posts_options');
		add_action('admin_enqueue_scripts',array($this,'admin_style'),10,0);
	}
	/* setup admin page */
	public function admin_style() {
		wp_register_style( 'avanced_related_posts_admin_css', plugins_url('/admin.css', __FILE__), array(), false, 'all' );
		wp_enqueue_style('avanced_related_posts_admin_css');
	}
	public function admin_menu() {
		$options = get_option('advanced_related_posts_options');
		add_submenu_page( 'themes.php', __('Advanced Related Posts Settings','advanced_related_posts'), __('Related Posts','advanced_related_posts'), 'edit_theme_options', 'avanced_related_posts_options_page', array($this,'admin_page'));
		/* sections */
		$custom_post_types = get_post_types(array('public' => true, '_builtin' => false),'objects','and');
		$post_post_type = get_post_type_object('post');
		$post_types = (!empty($custom_post_types) && is_array($custom_post_types)) ? array_merge(array($post_post_type),$custom_post_types) : array($post_post_type);
		foreach($post_types as $post_type) {
			$label = (!empty($post_type->labels->singular_name)) ? "{$post_type->labels->singular_name}" : "{$post_type->name}";
			add_settings_section("avanced_related_posts_options_{$post_type->name}_section", sprintf(__('%s Options','advanced_related_posts'),$label),create_function("","echo '<p class=\'description\'>'. sprintf(__('Related posts display options for <code>%s</code> post type','advanced_related_posts'),'{$label}') .'</p>';"),'avanced_related_posts_options_page');
			/* fields */
			add_settings_field( "{$post_type->name}display", __('Display related posts?','advanced_related_posts'), array($this,'avanced_related_posts_options_display_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}filter_order", __('Filter order','advanced_related_posts'), array($this,'avanced_related_posts_options_filter_order_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}box_html_id", __('Related Posts wrapper HTML id','advanced_related_posts'), array($this,'avanced_related_posts_options_box_html_id_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}box_css_class", __('Related Posts wrapper CSS class','advanced_related_posts'), array($this,'avanced_related_posts_options_box_css_class_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}posts_per_page", __('How many posts to display?','advanced_related_posts'), array($this,'avanced_related_posts_options_posts_per_page_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}post_type", __('Post type to use','advanced_related_posts'), array($this,'avanced_related_posts_options_post_type_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}taxonomy", __('Taxonomy to use','advanced_related_posts'), array($this,'avanced_related_posts_options_taxonomy_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}introduction_text", __('Introduction text','advanced_related_posts'), array($this,'avanced_related_posts_options_introduction_text_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}introduction_html_tag", __('Introduction wrapper HTML tag','advanced_related_posts'), array($this,'avanced_related_posts_options_introduction_html_tag_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}introduction_css_class", __('Introduction wrapper CSS class','advanced_related_posts'), array($this,'avanced_related_posts_options_introduction_css_class_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}thumbnail_display", __('Display post thumbnails?','advanced_related_posts'), array($this,'avanced_related_posts_options_thumbnail_display_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}thumbnail_size", __('Thumbnail size','advanced_related_posts'), array($this,'avanced_related_posts_options_thumbnail_size_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}thumbnail_custom_size", __('Thumbnail custom size','advanced_related_posts'), array($this,'avanced_related_posts_options_thumbnail_custom_size_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}thumbnail_css_class", __('Thumbnail CSS class','advanced_related_posts'), array($this,'avanced_related_posts_options_thumbnail_css_class_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}thumbnail_search", __('Enable thumbnail search?','advanced_related_posts'), array($this,'avanced_related_posts_options_thumbnail_search_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}thumbnail_search_meta_fields", __('Custom fields to search in','advanced_related_posts'), array($this,'avanced_related_posts_options_thumbnail_search_meta_fields_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}thumbnail_default_url", __('Default thumbnail URL','advanced_related_posts'), array($this,'avanced_related_posts_options_thumbnail_default_url_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}title_display", __('Display post titles?','advanced_related_posts'), array($this,'avanced_related_posts_options_title_display_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}title_max_length", __('Title length limit','advanced_related_posts'), array($this,'avanced_related_posts_options_title_max_length_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}link_title_prefix", __('Link title prefix text','advanced_related_posts'), array($this,'avanced_related_posts_options_link_title_prefix_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}link_css_class", __('Link CSS class','advanced_related_posts'), array($this,'avanced_related_posts_options_link_css_class_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}link_relation", __('Link relation attribute','advanced_related_posts'), array($this,'avanced_related_posts_options_link_relation_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
			add_settings_field( "{$post_type->name}link_inline", __('Other link inline properties','advanced_related_posts'), array($this,'avanced_related_posts_options_link_inline_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_{$post_type->name}_section", array('post_type' => $post_type->name, 'options' => $options));
		}
		add_settings_section("avanced_related_posts_options_css_section", __('CSS Options','advanced_related_posts'),create_function("","echo '<p class=\'description\'>'. __('Here you may disable Related Posts stylesheet in order to use your own styling.','advanced_related_posts') .'</p>';"),'avanced_related_posts_options_page');
		add_settings_field( "css_hide", __('Related Posts style sheet','advanced_related_posts'), array($this,'avanced_related_posts_options_css_hide_field'), 'avanced_related_posts_options_page', "avanced_related_posts_options_css_section", array('options' => $options));
	}
	/* fields */
	public function avanced_related_posts_options_css_hide_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$label = __('Do not load Related Posts style sheet on the front end.','advanced_related_posts');
		$description = __('Check this box if you do not want to load an extra style sheet by this plugin and you wish to use your own styles within <code>style.css</code> file of your theme.','advanced_related_posts');
		$checked = (!empty($options['css_hide'])) ? " checked=\"checked\"" : "";
		echo "<input type=\"checkbox\" name=\"advanced_related_posts_options[css_hide]\" value=\"1\"{$checked}>&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_display_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Display related posts after post content?','advanced_related_posts');
		$checked = (!empty($option['display'])) ? " checked=\"checked\"" : "";
		echo "<input type=\"checkbox\" name=\"advanced_related_posts_options[{$post_type}][display]\" value=\"1\"{$checked}>&nbsp;{$label}";
	}
	public function avanced_related_posts_options_filter_order_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('If you wish to change the place where the related posts are displayed after your posts, you may change the number here. Defaults to <code>11</code>.','advanced_related_posts');
		$value = (!empty($option['filter_order'])) ? esc_attr(strip_tags(trim($option['filter_order']))) : "11";
		echo "<input type=\"number\" step='1' min='0' max=\"99\" name=\"advanced_related_posts_options[{$post_type}][filter_order]\" value=\"{$value}\" class=\"small-text code\" />&nbsp;{$label}";
	}
	public function avanced_related_posts_options_box_html_id_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('You may enter here a unique ID for the related posts wrapper if you need this for your customizations.','advanced_related_posts');
		$value = (!empty($option['box_html_id'])) ? esc_attr(strip_tags(trim($option['box_html_id']))) : "";
		echo "<input type=\"text\" placeholder=\"related_posts_box\" name=\"advanced_related_posts_options[{$post_type}][box_html_id]\" value=\"{$value}\" class=\"long-text code\" />&nbsp;{$label}";
	}
	public function avanced_related_posts_options_box_css_class_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here CSS classes for Related Posts wrapper','advanced_related_posts');
		$description = __('Separate multiples CSS classes with spaces like this : <code>class_one class_two</code>','advanced_related_posts');
		$value = (!empty($option['box_css_class'])) ? esc_attr(strip_tags(trim($option['box_css_class']))) : "";
		echo "<input type=\"text\" name=\"advanced_related_posts_options[{$post_type}][box_css_class]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_posts_per_page_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Please, specify how many related posts to display','advanced_related_posts');
		$value = (!empty($option['posts_per_page'])) ? esc_attr(strip_tags(trim($option['posts_per_page']))) : "3";
		echo "<input type=\"number\" step='1' min='2' max=\"7\" name=\"advanced_related_posts_options[{$post_type}][posts_per_page]\" value=\"{$value}\" class=\"small-text code\" />&nbsp;{$label}";
	}
	public function avanced_related_posts_options_post_type_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Please, select the post type to use.','advanced_related_posts');
		$custom_post_types = get_post_types(array('public' => true, '_builtin' => false),'objects','and');
		$post_post_type = get_post_type_object('post');
		$post_types = (!empty($custom_post_types) && is_array($custom_post_types)) ? array_merge(array($post_post_type),$custom_post_types) : array($post_post_type);
		if(!empty($post_types) && is_array($post_types) && !is_wp_error($post_types)) {
			$input = "";
			$option['post_type'] = (!empty($option['post_type'])) ? $option['post_type'] : "{$post_type}";
			foreach($post_types as $type) {
				$name = (!empty($type->labels->singular_name)) ? "{$type->labels->singular_name}" : "{$type->name}" ;
				$selected = ("{$type->name}" == $option['post_type']) ? " selected=\"selected\"" : "";
				$input .= "<option value=\"{$type->name}\"{$selected} >{$name}</option>\n";
			}
			echo "<select name=\"advanced_related_posts_options[{$post_type}][post_type]\">\n{$input}</select>&nbsp;{$label}";
		}
	}
	public function avanced_related_posts_options_taxonomy_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Please, select the taxonomy to use.','advanced_related_posts');
		$taxonomies = get_object_taxonomies("{$post_type}","objects");
		if(!empty($taxonomies) && is_array($taxonomies) && !is_wp_error($taxonomies)) {
			$input = "";
			foreach($taxonomies as $taxonomy) {
				$name = (!empty($taxonomy->labels->singular_name)) ? "{$taxonomy->labels->singular_name}" : "{$taxonomy->name}" ;
				$selected = (!empty($option['taxonomy']) && "{$taxonomy->name}" == $option['taxonomy']) ? " selected=\"selected\"" : "";
				$input .= "<option value=\"{$taxonomy->name}\"{$selected} >{$name}</option>\n";
			}
			echo "<select name=\"advanced_related_posts_options[{$post_type}][taxonomy]\">\n{$input}</select>&nbsp;{$label}";
		}
	}
	public function avanced_related_posts_options_introduction_text_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here the text to display before the related posts','advanced_related_posts');
		$description = __('This text will be wrapped in the HTML tag you specify below.','advanced_related_posts');
		$value = (!empty($option['introduction_text'])) ? esc_attr(trim($option['introduction_text'])) : "";
		echo "<input type=\"text\" placeholder=\"These posts may interest you\" name=\"advanced_related_posts_options[{$post_type}][introduction_text]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_introduction_html_tag_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Please, select the HTML tag to use for the introduction text wrapper.','advanced_related_posts');
		$tags = array(
			'p' => "&lt;p&gt;...&lt;/p&gt;",
			'h2' => "&lt;h2&gt;...&lt;/h2&gt;",
			'h3' => "&lt;h3&gt;...&lt;/h3&gt;",
			'h4' => "&lt;h4&gt;...&lt;/h4&gt;",
			'h5' => "&lt;h5&gt;...&lt;/h5&gt;",
			'h6' => "&lt;h6&gt;...&lt;/h6&gt;",
		);
		$input = "";
		foreach($tags as $tag => $name) {
			$selected = (!empty($option['introduction_html_tag']) && $tag == $option['introduction_html_tag']) ? " selected=\"selected\"" : "";
			$input .= "<option value=\"{$tag}\"{$selected} >{$name}</option>\n";
		}
		echo "<select name=\"advanced_related_posts_options[{$post_type}][introduction_html_tag]\">\n{$input}</select>&nbsp;{$label}";
	}
	public function avanced_related_posts_options_introduction_css_class_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here CSS classes for introduction text wrapper','advanced_related_posts');
		$description = __('Separate multiples CSS classes with spaces like this : <code>class_one class_two</code>','advanced_related_posts');
		$value = (!empty($option['introduction_css_class'])) ? esc_attr(strip_tags(trim($option['introduction_css_class']))) : "";
		echo "<input type=\"text\" name=\"advanced_related_posts_options[{$post_type}][introduction_css_class]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_thumbnail_display_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Display related posts thumbnails? <small>(recommended)</small>','advanced_related_posts');
		$checked = (!empty($option['thumbnail_display'])) ? " checked=\"checked\"" : "";
		echo "<input type=\"checkbox\" name=\"advanced_related_posts_options[{$post_type}][thumbnail_display]\" value=\"1\"{$checked}>&nbsp;{$label}";
	}
	public function avanced_related_posts_options_thumbnail_size_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		global $_wp_additional_image_sizes;
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Please, select the image size for thumbnails.','advanced_related_posts');
		$image_sizes = array('thumbnail', 'medium', 'large', 'full');
		if ( isset( $_wp_additional_image_sizes ) && (count( $_wp_additional_image_sizes ) != 0) ) {
			$extra_sizes = $_wp_additional_image_sizes;
			$image_sizes = array_merge( $image_sizes, array_keys( $extra_sizes ) );
		}
		foreach($image_sizes as $size) {
			$size_options[$size] = str_replace('_',' ',str_replace('-',' ',$size)) ;
		}
		$size_options['custom'] = __('custom size (not recommended)','advanced_related_posts');
		$input = "";
		foreach($size_options as $size => $name) {
			$selected = (!empty($option['thumbnail_size']) && $size == $option['thumbnail_size']) ? " selected=\"selected\"" : "";
			$input .= "<option value=\"{$size}\"{$selected} >{$name}</option>\n";
		}
		echo "<select name=\"advanced_related_posts_options[{$post_type}][thumbnail_size]\">\n{$input}</select>&nbsp;{$label}";
	}
	public function avanced_related_posts_options_thumbnail_custom_size_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$values = (!empty($option['thumbnail_custom_size']) && is_array($option['thumbnail_custom_size'])) ? $option['thumbnail_custom_size'] : array();
		$i = 0;
		foreach(array('Width','Height') as $prop) {
			$label = __("{$prop}");
			echo "<label>$label</label>&nbsp;:&nbsp;<input type='number' placeholder='75' step='1' min='50' name='advanced_related_posts_options[{$post_type}][thumbnail_custom_size][{$i}]' value='{$values[$i]}' class='small-text code' />&nbsp;";
			$i++;
		}
		echo '<br /><span class="description">'. __('If you have chosen the custom size for thumbnails above, please, specify the size to use.','advanced_related_posts') .'</span>';
	}
	public function avanced_related_posts_options_thumbnail_css_class_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here CSS classes for related posts thumbnails','advanced_related_posts');
		$description = __('Separate multiples CSS classes with spaces like this : <code>class_one class_two</code>','advanced_related_posts');
		$value = (!empty($option['thumbnail_css_class'])) ? esc_attr(strip_tags(trim($option['thumbnail_css_class']))) : "";
		echo "<input type=\"text\" name=\"advanced_related_posts_options[{$post_type}][thumbnail_css_class]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_thumbnail_search_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enable search thumbnail functionality? <small>(recommended)</small>','advanced_related_posts');
		$description = __('If checked, in case one of the related posts has no thumbnail, the plugin will try to find an image associated with this post by checking the data in the post meta fields and searching for images in the post content. If nothing found, the plugin will display a default thumbnail.','advanced_related_posts');
		$checked = (!empty($option['thumbnail_search'])) ? " checked=\"checked\"" : "";
		echo "<input type=\"checkbox\" name=\"advanced_related_posts_options[{$post_type}][thumbnail_search]\" value=\"1\"{$checked}>&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_thumbnail_search_meta_fields_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here the comma separated list of custom field names to search image URL in.','advanced_related_posts');
		$description = __('It can be something like : <code>thesis_post_image,thesis_tumb,custom_thumbnail_url</code>. The custom field values must be strings that may contain the URL of an image in your media library.','advanced_related_posts');
		$value = (!empty($option['search_meta_fields'])) ? esc_attr(strip_tags(trim($option['search_meta_fields']))) : "";
		echo "<input type=\"text\" name=\"advanced_related_posts_options[{$post_type}][search_meta_fields]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_thumbnail_default_url_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here the full URL of default thumbnail to use.','advanced_related_posts');
		$description = __('The URL must be full (contain <code>http://</code> or <code>https://</code>) and must point to an image file uploaded to your media library. No need to give the exact size image URL as the plugin will take care of it automatically. If thumbnails search functionality is enabled, this field is <b>mandatory</b>.','advanced_related_posts');
		$value = (!empty($option['thumbnail_default_url'])) ? esc_attr(strip_tags(trim($option['thumbnail_default_url']))) : "";
		echo "<input type=\"text\" placeholder=\"http://yoursite.com/wp-content/uploads/2011/07/default_thumbnail.jpg\" name=\"advanced_related_posts_options[{$post_type}][thumbnail_default_url]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_title_display_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Display related posts titles? <small>(recommended)</small>','advanced_related_posts');
		$checked = (!empty($option['title_display'])) ? " checked=\"checked\"" : "";
		echo "<input type=\"checkbox\" name=\"advanced_related_posts_options[{$post_type}][title_display]\" value=\"1\"{$checked}>&nbsp;{$label}";
	}
	public function avanced_related_posts_options_title_max_length_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('If you wish to limit the post title length to a certain amount of characters, enter the number here. <code>0</code> stands for no limit.','advanced_related_posts');
		$value = (!empty($option['title_max_length'])) ? esc_attr(strip_tags(trim($option['title_max_length']))) : "";
		echo "<input type=\"number\" placeholder=\"0\" step='1' min='0' name=\"advanced_related_posts_options[{$post_type}][title_max_length]\" value=\"{$value}\" class=\"small-text code\" />&nbsp;{$label}";
	}
	public function avanced_related_posts_options_link_title_prefix_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here the text to add to link title attribute before the post title','advanced_related_posts');
		$description = __('Mind the space after it.','advanced_related_posts');
		$value = (!empty($option['link_title_prefix'])) ? esc_attr(strip_tags($option['link_title_prefix'])) : "";
		echo "<input type=\"text\" placeholder=\"Read \" name=\"advanced_related_posts_options[{$post_type}][link_title_prefix]\" value=\"{$value}\" class=\"long-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_link_css_class_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here CSS classes for related posts links','advanced_related_posts');
		$description = __('Separate multiples CSS classes with spaces like this : <code>class_one class_two</code>','advanced_related_posts');
		$value = (!empty($option['link_css_class'])) ? esc_attr(strip_tags(trim($option['link_css_class']))) : "";
		echo "<input type=\"text\" name=\"advanced_related_posts_options[{$post_type}][link_css_class]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_link_relation_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('Enter here post link relation attributes','advanced_related_posts');
		$description = __('Separate multiple values with spaces like this : <code>bookmark external</code>','advanced_related_posts');
		$value = (!empty($option['link_relation'])) ? esc_attr(strip_tags(trim($option['link_relation']))) : "";
		echo "<input type=\"text\" placeholder=\"bookmark\" name=\"advanced_related_posts_options[{$post_type}][link_relation]\" value=\"{$value}\" class=\"large-text code\" />&nbsp;{$label}";
		echo "<br /><span class=\"description\">{$description}</span>";
	}
	public function avanced_related_posts_options_link_inline_field($data = null) {
		if(empty($data)) return;
		extract($data, EXTR_SKIP);
		$options = (is_array($options)) ? $options : array();
		$option = $options["{$post_type}"];
		$label = __('If you wish to add some other inline properties for the post links you may enter them here.','advanced_related_posts');
		$value = (!empty($option['options_link_inline'])) ? esc_attr(strip_tags(trim($option['options_link_inline']))) : "";
		echo "<input type=\"text\" name=\"advanced_related_posts_options[{$post_type}][options_link_inline]\" value=\"{$value}\" class=\"large-text code\" /><br />{$label}";
	}
	/* admin page html */
	public function admin_page() {
?>
		<div class="wrap avanced_related_posts_options_page">
		<?php screen_icon(); ?>
		<h2><?php _e('Advanced Related Posts Settings','advanced_related_posts'); ?></h2>
		<form method="post" action="options.php">
<?php
		settings_fields('advanced_related_posts_group');
		ob_start();
		do_settings_sections('avanced_related_posts_options_page');
		$page = ob_get_contents();
		ob_end_clean();
		echo avanced_related_posts_admin_page_wrapper($page);
?>
		<?php submit_button(); ?>
		</form>
		<h3><?php _e('Affiliate Program','advanced_related_posts'); ?></h3>
		<p><?php _e('Like this plugin? How about making some money while introducing this great tool to your readers? Click the button to subscribe as affiliate and earn up to 50% of sales though your affiliate link.','advanced_related_posts'); ?> <a href="http://thesistut.com/make-money/" target="_blank" class="button"><?php _e('Affiliate Program','advanced_related_posts'); ?></a></p>
		</div>
<?php
	}
	/* metabox */
	public function add_meta_boxes() {
		$custom_post_types = get_post_types(array('public' => true, '_builtin' => false),'names','and');
		$post_types = (!empty($custom_post_types) && is_array($custom_post_types)) ? array_merge(array('post'),$custom_post_types) : array('post');
		if(!empty($post_types) && is_array($post_types) && !is_wp_error($post_types)) {
			foreach($post_types as $post_type) {
				add_meta_box( 'avanced_related_posts', __( 'Advanced Ralated Posts','advanced_related_posts'),array($this,'metabox'), $post_type,'side','low');
			}
		}
	}
	public function metabox($post) {
		wp_nonce_field(plugin_basename( __FILE__ ),'avanced_related_posts_noncename');
		$fields = array(
			'_avanced_related_posts_hide' => __('Do not display related posts on this post','advanced_related_posts')
		);
		foreach($fields as $field => $label) {
			$checked = ("" != get_post_meta($post->ID, $field, true)) ? " checked='checked'" : "";
			echo "<p><input type='checkbox' name='{$field}' value='1'{$checked} />&nbsp;{$label}</p>";
		}
	}
	public function save_post($post_id) {
		if(isset($_POST['avanced_related_posts_noncename'])) {
			if ( !wp_verify_nonce( $_POST['avanced_related_posts_noncename'], plugin_basename(__FILE__) ))
				return $post_id;
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;
			$keys = array(
				'_avanced_related_posts_hide'
			);
			foreach($keys as $key) {
				$value = (isset($_POST[$key]) && !empty($_POST[$key])) ? $_POST[$key] : '';
				$current_value = get_post_meta($post_id, $key,true);
				if (!empty($value) && $value != $current_value) {
					update_post_meta($post_id,$key,$value);
				} elseif (empty($value) && $current_value) {
					delete_post_meta($post_id,$key,$current_value);
				}
			}
			return $post_id;
		}
		return $post_id;
	}
	/* END ADMIN */
}
$advanced_related_posts = new advanced_related_posts ;

/* HELPERS */
function avanced_related_posts_admin_page_wrapper($content) {
	$content = explode('<h3>', $content);
	$out = $script = "";
	$count = 1 ;
	unset($content[0]);
	foreach($content as $section) {
		$class = ($count === 1) ? ' first' : '';
		$section = "<div class=\"postbox{$class}\"><h3 id=\"avanced_related_posts_section_{$count}_title\" title=\"". __('Click to toggle') ."\"><span class=\"toggle\"></span> " . $section ;
		$injection = "\n<div id=\"avanced_related_posts_section_{$count}_content\" class=\"avanced_related_posts_section_content\">";
		$out .= str_replace('</h3>',"</h3>{$injection}",$section) . "</div>\n</div>";
		$script .= "
jQuery(document).ready(function($){
	$('#avanced_related_posts_section_{$count}_title').on('click', function(){
		$('#avanced_related_posts_section_{$count}_content').slideToggle(630);
		$('#avanced_related_posts_section_{$count}_title').toggleClass('active');
	});
});
		";
		$count ++;
	}
	$script = "<script type='text/javascript'>
/* <![CDATA[ */
{$script}
/* ]]> */
</script>";
	return $script . $out ;
}
function advanced_related_posts_shorten_title($input = null, $length = 100, $ellipses = true, $strip_html = true ) {
	if ($strip_html) {
		$input = strip_tags($input);
	}
	if (strlen($input) <= $length) {
		return $input;
	}
	$last_space = strrpos(substr($input, 0, $length), ' ');
	$trimmed_text = substr($input, 0, $last_space);
	if ($ellipses) {
		$trimmed_text .= ' ...';
	}
	return $trimmed_text;
}