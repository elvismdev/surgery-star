=== Pinterest Verify ===
Contributors: pderksen
Tags: pinterest, pinterest verify, pinterest verification, meta tag, verify, verification
Requires at least: 3.4.2
Tested up to: 3.6.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Verify your website with Pinterest and show pinners you're a trustworthy source!

== Description ==

This plugin allows you to verify your website with Pinterest by inserting a meta tag on your front page (no coding required).

It simply outputs the verification meta tag for Pinterest to recognize your website as instructed at <http://business.pinterest.com/verify/>.

Now pinners will see the verification checkmark AND grant you access to Pinterest's free analytics!

That's all there is to it!

[Follow this project on Github](https://github.com/pderksen/WP-Pinterest-Verify).

== Installation ==

**Finding and installing through the WordPress admin:**

1. If searching for this plugin in your WordPress admin, under Plugins > Add New search for "Pinterest verify".
1. Find the plugin that's labeled **Pinterest Verify**.
1. Also look for the author name **Phil Derksen**.
1. Click "Install Now", then Activate, then head to Settings > Pinterest Verify.

**Alternative installation methods:**

* Download this plugin, then upload through the WordPress admin (Plugins > Add New > Upload)
* Download this plugin, unzip the contents, then FTP upload to the `/wp-content/plugins/` directory

== Frequently Asked Questions ==

= Troubleshooting =

If you have any caching plugins or options running make sure those are cleared before verifying.

See the plugin settings page for further instructions.

== Screenshots ==

1. Settings page. Just a single textbox.
2. Sample HTML source meta tag output on public front page.

== Changelog ==

= 1.0.0 =
* Initial release.
